// Local imports
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import "./App.css";

// Components Imports
import Navbar from "./components/Navbar";

// Pages Imports
import Login from "./pages/Login";
import Register from "./pages/Register";
import Main from "./pages/Main";
import NotFound from "./pages/NotFound";
import Logout from "./pages/Logout";
import { useEffect, useState } from "react";
import { UserProvider } from "./UserContext";

export default function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data._id !== undefined) {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
          });
        } else {
          setUser({
            id: null,
            isAdmin: data.isAdmin,
          });
        }
      });
  }, []);

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <div className="container">
          <div className="header">
            <Navbar />
          </div>
          <div className="contents">
            <Routes>
              {user.id !== null ? (
                <>
                  <Route exact path="/home" element={<Main />} />
                  <Route exact path="/logout" element={<Logout />} />
                </>
              ) : (
                <>
                  <Route exact path="/" element={<Login />} />
                  <Route exact path="/register" element={<Register />} />
                </>
              )}
              <>
                <Route exact path="/" element={<Login />} />
                <Route exact path="*" element={<NotFound />} />
              </>
            </Routes>
          </div>
        </div>
      </Router>
    </UserProvider>
  );
}
