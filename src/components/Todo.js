import { Link } from "react-router-dom";
import { useState } from "react";
import Swal from "sweetalert2";
import { motion } from "framer-motion";

export default function Todo({ todos, deleteTodo, updateTodo }) {
  console.log(todos);
  const { _id, description, title, status, postedBy } = todos;

  const done = {
    color: "#188351",
    fontWeight: 500,
  };

  const undone = {
    color: "#ffac4d",
    fontWeight: 500,
  };
  return (
    <>
      <motion.div
        whileHover={{ scale: 1.00125 }}
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 0.125 }}
        className={status ? "card done" : "card"}
      >
        <div className="card--header">
          <div className="card--header-left">
            <p>Author: {postedBy}</p>
          </div>
          <div className="card--header-right">
            <span className="delete" onClick={() => deleteTodo(_id, title)}>
              <i className="bx bxs-trash"></i>
            </span>

            <span
              className="update"
              value={status}
              onClick={() => updateTodo(_id, title, status)}
            >
              {status ? (
                <i className="bx bxs-bulb todo-done"></i>
              ) : (
                <i className="bx bx-bulb todo-undone"></i>
              )}
            </span>
          </div>
        </div>
        <div className="card--body">
          <div className="card--title">
            <h1 style={status ? done : undone}>{title}</h1>
          </div>
          <div className="card--description">
            <p>{description}</p>
            <p style={status ? done : undone}>{status ? "Done" : " Pending"}</p>
          </div>
        </div>
      </motion.div>
    </>
  );
}
