import { NavLink } from "react-router-dom";
import { useContext, useState } from "react";
import Logo from "../images/headerLogo.png";
import UserContext from "../UserContext";

export default function Navbar() {
  const [isShown, setIsShown] = useState(false);
  const { user } = useContext(UserContext);
  let style = {
    textDecoration: "underline",
  };
  return (
    <>
      <nav className="nav">
        <div className="nav--left">
          <img src={Logo} alt="" />
        </div>
        <div className="nav--right">
          <ul className="nav-list">
            {user.id !== null ? <NavLink to="/logout">Logout</NavLink> : <></>}
          </ul>
        </div>
      </nav>
    </>
  );
}
