import { Link, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import Swal from "sweetalert2";
const validator = require("validator");

export default function Register() {
  const navigate = useNavigate();

  const initialValues = {
    username: "",
    email: "",
    password: "",
    cpassword: "",
  };

  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState([]);
  // Toggle Show Password
  const [showPassword, setShowPassword] = useState(false);
  const [showCPassword, setShowCPassword] = useState(false);

  const [isActive, setIsActive] = useState(false);

  function toggleShowCPassword() {
    setShowCPassword((prevState) => !prevState);
  }
  function toggleShowPassword() {
    setShowPassword((prevState) => !prevState);
  }

  const inValid = {
    color: "#f74242",
  };

  const validate = (values) => {
    const errors = {};

    const passwordValidate =
      /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{6,}$/;

    if (!values.username) {
      errors.username = "Username is required!";
    } else if (values.username.length < 4) {
      errors.username = "Username must at least be 4 characters.";
    }
    if (!values.email) {
      errors.email = "Email is required!";
    } else if (!validator.isEmail(values.email)) {
      errors.email = "Email Address is not valid.";
    }
    if (!values.password) {
      errors.password = "Password is required!";
    } else if (!passwordValidate.test(values.password)) {
      errors.password = `Password must have:

      at least 6 Characters.
      at least 1 Special Character
      at least 1 Number
      at least 1 Uppercase Letter
      at least 1 Lowercase Letter.`;
    }
    if (!values.cpassword) {
      errors.cpassword = "Confirm Password is required!";
    } else if (values.password !== values.cpassword) {
      errors.cpassword = "Password does not match.";
    }
    setIsActive(true);
    return errors;
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  function registerUser(e) {
    e.preventDefault();

    setFormErrors(validate(formValues));

    if (Object.keys(formErrors).length === 0 && isActive) {
      fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email: formValues.email }),
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          if (data) {
            Swal.fire({
              title: "Duplicate email found",
              icon: "error",
              text: "Kindly provide another email to complete the registration.",
            });
          } else {
            fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify({
                userName: formValues.username,
                email: formValues.email,
                password: formValues.password,
              }),
            })
              .then((res) => res.json())
              .then((data) => {
                console.log(data);
                if (data) {
                  Swal.fire({
                    title: "Registration successful",
                    icon: "success",
                    text: "Welcome to Todo!",
                  });
                  setFormValues(initialValues);

                  navigate("/");
                } else {
                  Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again.",
                  });
                }
              });
          }
        });
    }
  }

  return (
    <div className="login--container">
      <form onSubmit={(e) => registerUser(e)}>
        <div className="input--group">
          <h2>Register</h2>
        </div>

        <div className="input--group">
          <input
            type="text"
            name="username"
            value={formValues.username}
            maxLength={25}
            onChange={handleChange}
            className="form--input"
            placeholder="User Name"
          />
          <p style={inValid}>{formErrors.username}</p>
        </div>
        <div className="input--group">
          <input
            type="email"
            value={formValues.email}
            name="email"
            // style={isEmailValid ? valid : inValid}
            maxLength={40}
            onChange={handleChange}
            className="form--input"
            placeholder="Email Address"
          />
          <p style={inValid}>{formErrors.email}</p>
        </div>

        <div className="input--group">
          <input
            type={showPassword ? "text" : "password"}
            value={formValues.password}
            name="password"
            // style={isPasswordValid ? valid : inValid}
            minLength={6}
            onChange={handleChange}
            className="form--input"
            placeholder="Password"
          />
          <pre style={inValid}>{formErrors.password}</pre>
          <div className="showpassword-toggle" onClick={toggleShowPassword}>
            {showPassword ? (
              <i className="bx bxs-hide"></i>
            ) : (
              <i className="bx bxs-show"></i>
            )}
          </div>
        </div>

        <div className="input--group">
          <input
            type={showCPassword ? "text" : "password"}
            value={formValues.cpassword}
            name="cpassword"
            // style={isCpasswordValid ? valid : inValid}
            minLength={6}
            onChange={handleChange}
            className="form--input"
            placeholder="Confirm Password"
          />
          <p style={inValid}>{formErrors.cpassword}</p>
          <div className="showpassword-toggle" onClick={toggleShowCPassword}>
            {showCPassword ? (
              <i className="bx bxs-hide"></i>
            ) : (
              <i className="bx bxs-show"></i>
            )}
          </div>
        </div>

        <div className="input--group">
          <button type="submit" className="submit" id="submitBtn">
            Register
          </button>
        </div>
        <p>
          Already Registered? <Link to="/">Login</Link>
        </p>
      </form>
    </div>
  );
}
