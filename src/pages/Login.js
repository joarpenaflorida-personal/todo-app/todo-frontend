import { Link, Navigate, useNavigate } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function Login() {
  const initialValues = {
    email: "",
    password: "",
  };
  const { user, setUser } = useContext(UserContext);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [formErrors, setFormErrors] = useState([]);
  const [formValues, setFormValues] = useState(initialValues);

  function toggleShowPassword() {
    setShowPassword((prevState) => !prevState);
  }

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  function loginUser(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data.accessToken);
        if (data.accessToken !== undefined) {
          localStorage.setItem("token", data.accessToken);
          retrieveUserDetails(data.accessToken);
          Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: "Welcome to Todo!",
          });
        } else {
          Swal.fire({
            title: "Authetication Failed!",
            icon: "error",
            text: "Check your login details and try again.",
          });
        }
      });
    setEmail("");
    setPassword("");
  }

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
          name: data.userName,
        });
      });
  };

  return user.id !== null ? (
    <Navigate to="/home" end />
  ) : (
    <>
      <div className="login--container">
        <form onSubmit={(e) => loginUser(e)}>
          <div className="input--group">
            <h2>Login</h2>
          </div>

          <div className="input--group">
            <input
              type="email"
              autoComplete="on"
              name="email"
              value={email}
              required
              onChange={(e) => setEmail(e.target.value)}
              className="form--input"
              placeholder="Email Address"
            />
          </div>
          <div className="input--group">
            <input
              type={showPassword ? "text" : "password"}
              autoComplete="on"
              name="password"
              value={password}
              required
              onChange={(e) => setPassword(e.target.value)}
              className="form--input"
              placeholder="Password"
            />
            <div className="showpassword-toggle" onClick={toggleShowPassword}>
              {showPassword ? (
                <i className="bx bxs-hide"></i>
              ) : (
                <i className="bx bxs-show"></i>
              )}
            </div>
          </div>
          <div className="input--group">
            {isActive ? (
              <button type="submit" className="submit">
                Submit
              </button>
            ) : (
              <button disabled type="submit" className="submit">
                Login
              </button>
            )}
          </div>
          <p>
            Not yet registered? <Link to="/register">Register</Link>
          </p>
        </form>
      </div>
    </>
  );
}
