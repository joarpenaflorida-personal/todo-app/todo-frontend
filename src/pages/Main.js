import UserContext from "../UserContext";
import { useContext, useState, useEffect } from "react";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import Todo from "../components/Todo";
import Sound from "../sound/ding.mp3";
import useSound from "use-sound";

export default function Main() {
  const { user } = useContext(UserContext);

  const [playSound] = useSound(Sound, { volume: 0.5 });

  const [showAddTodo, setShowAddTodo] = useState(false);

  const [todosDone, setTodosDone] = useState([]);

  const [todosPending, setTodosPending] = useState([]);

  const [filtered, setFiltered] = useState(false);

  const [title, setTitle] = useState("");

  const [isActve, setIsActive] = useState(false);

  const [description, setDescription] = useState("");

  useEffect(() => {
    if (title !== "" && description !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [title, description]);

  useEffect(() => {
    fetchData();
  }, []);

  function clearTodo() {
    fetch(`${process.env.REACT_APP_API_URL}/todos/remove`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            title: "Todo Cleared",
            icon: "success",
          });
          fetchData();
        }
      });
  }
  function updateTodo(id, title, status) {
    console.log(id, title);
    fetch(`${process.env.REACT_APP_API_URL}/todos/${id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        status: !status,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          if (!status) {
            playSound();
            Swal.fire({
              title: `${title} done.`,
              icon: "info",
            });
          } else {
            playSound();
            Swal.fire({
              title: `${title} pending.`,
              icon: "info",
            });
          }
          fetchData();
        } else {
          Swal.fire({
            title: `Something Went Wrong`,
            icon: "error",
          });
        }
      });
  }
  function deleteTodo(id, title) {
    console.log(id, title);
    fetch(`${process.env.REACT_APP_API_URL}/todos/${id}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          Swal.fire({
            title: `Delete Successful ${title}`,
            icon: "success",
          });
          fetchData();
        } else {
          Swal.fire({
            title: `Something went wrong`,
            icon: "error",
          });
        }
      });
  }

  const fetchData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/todos/`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        const isDone = data.filter((todo) => todo.status === true);
        const isPending = data.filter((todo) => todo.status === false);
        setTodosDone(
          isDone.map((todo) => {
            return (
              <Todo
                key={todo._id}
                todos={todo}
                deleteTodo={deleteTodo}
                updateTodo={updateTodo}
              />
            );
          })
        );
        setTodosPending(
          isPending.map((todo) => {
            return (
              <Todo
                key={todo._id}
                todos={todo}
                deleteTodo={deleteTodo}
                updateTodo={updateTodo}
              />
            );
          })
        );
      });
  };
  function toggleDone() {
    setFiltered(true);
  }
  function togglePending() {
    setFiltered(false);
  }
  function clearInput() {
    setTitle("");
    setDescription("");
  }
  function toggleShow() {
    setShowAddTodo((prevState) => !prevState);
    clearInput();
  }
  function handleSubmitAddTodo(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/todos/`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        title: title,
        description: description,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          Swal.fire({
            title: "Todo Added",
            icon: "success",
          });
          fetchData();
          toggleShow();
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
          });
        }
      });
  }
  return (
    <>
      <div className="todo-container">
        {showAddTodo && (
          <div className="todo-add">
            <div className="close" onClick={toggleShow}>
              <i className="bx bx-x"></i>
            </div>
            <form onSubmit={(e) => handleSubmitAddTodo(e)}>
              <div className="input--group">
                <h1>Add Todo</h1>
              </div>
              <div className="input--group">
                <input
                  type="text"
                  value={title}
                  maxLength={30}
                  onChange={(e) => setTitle(e.target.value)}
                  placeholder="Title"
                  required
                />
              </div>
              <div className="input--group">
                <input
                  type="text"
                  maxLength={50}
                  onChange={(e) => setDescription(e.target.value)}
                  placeholder="Description"
                  required
                />
              </div>
              <div className="input--group">
                {isActve ? (
                  <button type="submit">Submit</button>
                ) : (
                  <button type="submit" disabled>
                    Submit
                  </button>
                )}
              </div>
            </form>
          </div>
        )}

        <div className="todo--header">
          <h1>Todos</h1>
          <div className="todo--controls">
            <button className="add--todo" onClick={toggleShow}>
              <i className="bx bx-plus"></i>
              <p>Add</p>
            </button>
            {todosPending.length > 0 && (
              <button className="clear--todo" onClick={() => clearTodo()}>
                <i className="bx bxs-trash-alt"></i>
                <p>Clear</p>
              </button>
            )}
          </div>
        </div>
        <div className="filtering">
          <ul>
            <li onClick={togglePending}>
              {todosPending.length > 0 ? (
                <>
                  <span className="circle todos-pending">
                    {todosPending.length}
                  </span>
                  <span>Pending</span>
                </>
              ) : (
                <>
                  <span>Pending</span>
                </>
              )}
            </li>
            <li onClick={toggleDone}>
              {todosDone.length > 0 ? (
                <>
                  <span className="circle todos-done">{todosDone.length}</span>
                  <span>Done</span>
                </>
              ) : (
                <>
                  <span>Done</span>
                </>
              )}
            </li>
          </ul>
        </div>
        <div className="todo--pending">
          <div className="pending--todos">
            {filtered ? (
              todosDone.length === 1 ? (
                <h2>{todosDone.length} done todo</h2>
              ) : todosDone.length > 1 ? (
                <h2>{todosDone.length} done todos</h2>
              ) : (
                <h2>No todo found</h2>
              )
            ) : todosPending.length === 1 ? (
              <h2>{todosPending.length} pending todo</h2>
            ) : todosPending.length > 1 ? (
              <h2>{todosPending.length} pending todos</h2>
            ) : (
              <h2>No todo found</h2>
            )}
          </div>
        </div>

        {filtered ? todosDone : todosPending}
      </div>
    </>
  );
}
