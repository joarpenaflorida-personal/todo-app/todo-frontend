import { useContext } from "react";
import { Link } from "react-router-dom";
import UserContext from "../UserContext";
export default function NotFound() {
  const { user } = useContext(UserContext);
  return (
    <>
      {user.id !== null ? (
        <div className="notfound">
          <h1>Not Found</h1>
          <Link to="/home">Back to Dashboard</Link>
        </div>
      ) : (
        <div className="notfound">
          <h1>Not Found</h1>
          <Link to="/">Back to Login</Link>
        </div>
      )}
    </>
  );
}
